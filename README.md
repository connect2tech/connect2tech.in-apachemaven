# Apache Maven

## Java/Apache Maven: The Truth About Building Java Programs: Bruce E. Hilton

> The entire section 4, profiles is very important. You need to revisit it on need basis.

### ~~Section 2: Discovering the benefits of having a Parent POM~~

* Child needs to know about the parent. So far.

* We can have `<dependencies>` tag in parent. But it has disadvantage that all the children will get all dependencies.

**Parent**.

```xml
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-math3</artifactId>
            <version>3.2</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>2.5</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
</dependencyManagement>

<build>
    <pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
        </plugins>
    </pluginManagement>
</build>
```

**Child**

```xml
<dependencies>
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-core</artifactId>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <scope>test</scope>
    </dependency>
</dependencies>

<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>			
        </plugin>
    </plugins>
</build>
```


* Using `<pluginManagement/>`. **Important**.

### ~~Section 4: How to add conditional logic to the build~~

* Setting up the Example Projects: Nice concept to import maven projects

**Build Properties**

* User Defined Properties
* Built-In Maven Properties
* Environment Properties
* Java Properties

```xml
<properties>
    <my.prop>This is my property</my.prop>
    <java.complier.version>1.8</java.complier.version>
    <!-- build is platform dependent! -->
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
</properties>
```

**Taking more advantages of Properties**

* [https://www.mojohaus.org/exec-maven-plugin/](https://www.mojohaus.org/exec-maven-plugin/)
* [https://stackoverflow.com/questions/2472376/how-do-i-execute-a-program-using-maven](https://stackoverflow.com/questions/2472376/how-do-i-execute-a-program-using-maven)
Exec plugin
* [https://stackoverflow.com/questions/9846046/run-main-class-of-maven-project](https://stackoverflow.com/questions/9846046/run-main-class-of-maven-project)

```
mvn exec:java -Dexec.mainClass="com.example.Main"
```

```xml
<plugin>
  <groupId>org.codehaus.mojo</groupId>
  <artifactId>exec-maven-plugin</artifactId>
  <version>1.2.1</version>
  <executions>
    <execution>
      <phase>package</phase>
      <goals>
        <goal>java</goal>
      </goals>
    </execution>
  </executions>
  <configuration>
    <mainClass>com.example.Main</mainClass>
    <arguments>
      <argument>foo</argument>
      <argument>bar</argument>
    </arguments>
  </configuration>
</plugin>
```

**How Maven Profiles work**

* Profiles cannot be inherited

![image1](images/image1.png)

`C:\nc\DevOps-Architect\connect2tech.in-ApacheMaven\Bruce-Hilton\maven102-section2-complete-master\plusplus2-complete\pom.xml`

```xml

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>com.denofprogramming</groupId>
		<artifactId>parent2-complete</artifactId>
		<version>0.0.1-SNAPSHOT</version>
	</parent>
	<artifactId>plusplus2-complete</artifactId>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

	<dependencies>
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-core</artifactId>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<profiles>
		<profile>
			<id>output</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>exec-maven-plugin</artifactId>
						<configuration>
							<mainClass>com.denofprogramming.output.App</mainClass>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>output2</id>
			<activation>
				<property>
					<name>my.profileActive</name>
					<value>Circle</value>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>exec-maven-plugin</artifactId>
						<configuration>
							<mainClass>com.denofprogramming.output.App</mainClass>
							<arguments>
								<argument>${my.profileActive}</argument>
							</arguments>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>fileExistProfile</id>
			<activation>
				<!-- <file><exists>${java.home}/../lib/tools.jar</exists>	</file> -->
				<!-- <file> <missing>${java.home}/../lib/fred.jar</missing> </file> -->
			</activation>
			<properties>
				<!-- <my.prop>Java tools.jar exists!!</my.prop> -->
				<!-- <my.prop>Java fred.jar NOT exists!!</my.prop> -->
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>exec-maven-plugin</artifactId>
						<configuration>
							<mainClass>com.denofprogramming.output.App</mainClass>
							<arguments>
								<argument>${my.prop}</argument>
							</arguments>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>jdkExistProfile</id>
			<!-- <activation> <jdk>1.8</jdk> </activation> -->
			<!-- <activation> <jdk>[1.5,1.6]</jdk> </activation> -->
			<properties>
				<!-- <my.prop>Java jdk 1.8 being used!!</my.prop> -->
				<!-- <my.prop>Java jdk 1.5 to 1.8 being used!!</my.prop> -->
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>exec-maven-plugin</artifactId>
						<configuration>
							<mainClass>com.denofprogramming.output.App</mainClass>
							<arguments>
								<argument>${my.prop}</argument>
							</arguments>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>osExistProfile</id>
			<activation>
		<!-- 		<os>
					<family>windows</family>
				</os> -->
			</activation>
			<properties>
				<my.prop>Running on windows OS!!</my.prop>
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>exec-maven-plugin</artifactId>
						<configuration>
							<mainClass>com.denofprogramming.output.App</mainClass>
							<arguments>
								<argument>${my.prop}</argument>
							</arguments>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
```

`mvn help:active-profiles`

### Section 5: All about Multi-Module Projects

* We make use of module tag in parent project. If a command is executed on parent, it will also be executed in sub-projects

**Adding Modules**

![image2](images/image2.png)

* Creating multi-module project from eclipse.
* The submodule can be in any location. Need to give proper path.

**Maven's Reactor**

So what is the reactor how is Path maven that allows it to execute a goal on a set of modules or subprojects
Well modules are discrete units work. The reactor gathers them together and builds them simultaneously.
It's the reactor that makes multi module build possible. It computes a graph of dependencies between the modules  
drives the build or the from the graph and this is one of the main reasons why cyclic dependency is allowed and then  
executes goals on the modules.

```
[INFO] Reactor Summary:
[INFO] 
[INFO] parent3-initial 0.0.1-SNAPSHOT ..................... SUCCESS [  0.369 s]
[INFO] Pick a Number Application Initial .................. SUCCESS [  0.003 s]
[INFO] plusplus3-initial .................................. SUCCESS [  0.003 s]
[INFO] Random Number Generator Initial 0.0.1-SNAPSHOT ..... SUCCESS [  0.004 s]
```

* Cyclic reference issue

### ~~Section 6: Multi-Module Design and Organisation~~

It has `C:\nc\DevOps-Architect\connect2tech.in-ApacheMaven\Bruce-Hilton\teaching-academy-master` project. Can be used as flagship project.

---

connect2tech.in-Maven-Modules
=============================

## Maven TestNG

https://maven.apache.org/surefire/maven-surefire-plugin/examples/testng.html

## Maven Notes/Concepts

**Reactor**

The mechanism in Maven that handles multi-module projects is referred to as the reactor. This part of the Maven core does the following:

 - Collects all the available modules to build
 - Sorts the projects into the correct build order
 - Builds the selected projects in order

**Lifecycle**

Configure maven so as to run clean goal of clean project on the initialize phase of the default build life cycle.

```
<plugin>
	<artifactId>maven-clean-plugin</artifactId>
	<version>3.1.0</version>
	<executions>
		<execution>
			<id>auto-clean</id>
			<phase>initialize</phase>
			<goals>
				<goal>clean</goal>
			</goals>
		</execution>
	</executions>
</plugin>
```


---

## Maven Operations



---

### Archetypes ###
http://maven.apache.org/archetypes/index.html

| Archetype ArtifactIds	          | Description                                                                                                                                                                     |
|---------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| maven\-archetype\-archetype     | An archetype to generate a sample archetype\.                                                                                                                                   |
| maven\-archetype\-j2ee\-simple  | An archetype to generate a simplifed sample J2EE application\.                                                                                                                  |
| maven\-archetype\-plugin        | An archetype to generate a sample Maven plugin\.                                                                                                                                |
| maven\-archetype\-plugin\-site  | An archetype to generate a sample Maven plugin site\.                                                                                                                           |
| maven\-archetype\-portlet       | An archetype to generate a sample JSR\-268 Portlet\.                                                                                                                            |
| maven\-archetype\-quickstart	  | An archetype to generate a sample Maven project\.                                                                                                                               |
| maven\-archetype\-simple        | An archetype to generate a simple Maven project\.                                                                                                                               |
| maven\-archetype\-sit           | An archetype to generate a sample Maven site which demonstrates some of the supported document types like APT, Markdown, XDoc, and FML and demonstrates how to i18n your site\. |
| maven\-archetype\-site\-simple  | An archetype to generate a sample Maven site\.                                                                                                                                  |
| maven\-archetype\-site\-skin	  | An archetype to generate a sample Maven Site Skin\.                                                                                                                             |
| maven\-archetype\-webapp        | An archetype to generate a sample Maven Webapp project\.                                                                                                                        |


### Notes ###

- using ```<dependencyManagement>``` for parent child dependencies.
- using ```<pluginManagement>``` for parent child dependencies.


### Debugging ###

[how-debug-your-maven-build](https://dzone.com/articles/)

**Java Program**

- mvnDebug exec:java -Dexec.mainClass="com.mkyong.password.SayHello"
- Remote Java Application on correct port

**Serenity / Unit Testing**

- mvn -Dmaven.failsafe.debug clean test verify
- Debug UserRegistrationRunner as Remote Java Application with port coming from above

---

**Including source code jar in eclipse**

```
mvn --settings "D:\naresh.chaurasia\softwares\apache-maven-3.5.0\conf\settings.no.cert.xml" clean install dependency:sources -Dmaven.test.skip=true -DincludeArtifactIds=serenity-core -Dclassifier=javadoc
```

---

## Maven Plugins

### jsonschema2pojo-maven-plugin ###

**Converting JSON Schema to Java/POJO**

```
<plugin>
	<groupId>org.jsonschema2pojo</groupId>
	<artifactId>jsonschema2pojo-maven-plugin</artifactId>
	<version>0.5.1</version>
	<configuration>
		<sourceDirectory>${basedir}/src/main/resources/schema</sourceDirectory>
		<targetPackage>guru.springframework.model</targetPackage>
		<useCommonsLang3>true</useCommonsLang3>
	</configuration>
	<executions>
		<execution>
			<goals>
				<goal>generate</goal>
			</goals>
		</execution>
	</executions>
</plugin>

#mvn package

```

---


### maven-surefire-plugin ###

**Running TestNG testcase using maven**

```
<build>
	<plugins>
		<plugin>
			<groupId>org.apache.maven.plugins</groupId>
			<artifactId>maven-surefire-plugin</artifactId>
			<version>2.19.1</version>
			<configuration>
				<suiteXmlFiles>
					<suiteXmlFile>Suite.xml</suiteXmlFile>
				</suiteXmlFiles>
			</configuration>
		</plugin>
	</plugins>
</build>
```

**Class Loading and Forking in Maven Surefire**

https://maven.apache.org/surefire/maven-surefire-plugin/examples/class-loading.html
https://maven.apache.org/surefire/maven-surefire-plugin/examples/fork-options-and-parallel-execution.html


### maven-clean-plugin ###

Run clean goal during initialize phase.

```
	<project>
	  [...]
	  <build>
	    <plugins>
	      <plugin>
	        <artifactId>maven-clean-plugin</artifactId>
	        <version>3.1.0</version>
	        <executions>
	          <execution>
	            <id>auto-clean</id>
	            <phase>initialize</phase>
	            <goals>
	              <goal>clean</goal>
	            </goals>
	          </execution>
	        </executions>
	      </plugin>
	    </plugins>
	  </build>
	  [...]
	</project>
```

---



---

### Jar plugin ###

- https://maven.apache.org/plugins/maven-jar-plugin/examples/include-exclude.html
- https://maven.apache.org/plugins/maven-jar-plugin/
- Jar Plugin
	- Build Lifecycle - DEFAULT
	- Has two goals: jar:jar, jar:test-jar
	- Purpose is to build jars from complied artifacts and project resources
	- Can be configured for custom manifests, and to make executable jars.
	
```
	<project>
	  ...
	  <build>
	    <plugins>
	      ...
	      <plugin>
	        <groupId>org.apache.maven.plugins</groupId>
	        <artifactId>maven-jar-plugin</artifactId>
	        <version>3.2.0</version>
	        <configuration>
	          <includes>
	            <include>**/service/*</include>
	          </includes>
	        </configuration>
	      </plugin>
	      ...
	    </plugins>
	  </build>
	  ...
	</project>
```

---

### Source plugin ###

- Package source code
- Package Phase
- Overriden to later phase (in the below example, the jar creation for source code has been deferred to install phase)

```
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>3.2.0</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<phase>install</phase>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
```

---

### Javadoc plugin ###

- Package javadocs
- Package Phase
- Overriden to later phase
- Defaults
- Many customization options

---

---
### Profile Example ###

mvn package -Pprofile1

```
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.5</source>
					<target>1.5</target>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>profile1</id>
			<build>
				<plugins>

					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>exec-maven-plugin</artifactId>
						<version>1.2.1</version>
						<executions>
							<execution>
								<!-- mvn exec:java, Or -->
								<phase>package</phase>
								<goals>
									<goal>java</goal>
								</goals>
							</execution>
						</executions>
						<configuration>
							<mainClass>in.connect2tech.TheJava</mainClass>
							<arguments>
								<argument>foo</argument>
								<argument>bar</argument>
							</arguments>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
```

---

---

---

---

---

```
```
		
- Resources plugin
	- Build Lifecycle - DEFAULT
	- Has 3 goals - resources:resources, resources:testResources, resources:copy-resources
	- Purpose is to copy project resources to output directory (target dir)
	- Can be configured for encoding, source and target directories
	- Rather versatile configuration options for copying files during build processing
	
- Surefire plugin
	- Build Lifecycle - DEFAULT
	- Has one goal: surefire:test
	- The Surefire plugin is used to execute unit test of the project.
	- By default supports JUnit 3, JUnit 4, JUnit 5, and TestNG
		- Cucumber runs under JUnit, Spock compiles to JUnit byte code.
	- By default includes classes named:
		- `**/Test*.java; **/*Test.java; **/*Tests.java; **/*TestCase.java`
		


- Deploy Plugin
	- Build Lifecycle - DEFAULT
	- Has two goals - deploy:deploy, deploy:deploy-file
	- Purpose is to deploy project artifacts to remote Maven repositories
	- Often done in CI
	- Configuration is typically part of the Maven POM

- Site Plugin
	- Build Lifecycle - SITE
	- Has 7 goals:
		- site:site - Generate site for project
		- site:deploy - Deploy site via Wagon
		- site:run - Run Site locally using Jetty as web server
		- site:stage - generate site to a local staging directory
		- site:stage-deploy - Deploy site to remote staging location
		- site:attach-descriptor - adds site.xml (site map file used by search engines) to files for deployment
		- site:jar - bundles site into a jar for deployment to a repository
		- site:effective-site - generates the site.xml file

### maven plugins
		
**maven-dependency-plugin**: Copy Jars in specific folder/location.
```
<build>
	<plugins>
		<plugin>
			<groupId>org.apache.maven.plugins</groupId>
			<artifactId>maven-dependency-plugin</artifactId>
			<version>3.0.1</version>
			<executions>
				<execution>
					<id>copy-dependencies</id>
					<phase>clean</phase>
					<goals>
						<goal>copy-dependencies</goal>
					</goals>
					<configuration>
						<outputDirectory>${project.basedir}/jar-files</outputDirectory>
						<overWriteReleases>false</overWriteReleases>
						<overWriteSnapshots>false</overWriteSnapshots>
						<overWriteIfNewer>true</overWriteIfNewer>
					</configuration>
				</execution>
			</executions>
		</plugin>
	</plugins>
</build>
```

**jetty-maven-plugin**: Jetty Server for Web Applications
```
<build>
	<finalName>connect2tech.in-OnlineBanking</finalName>
	<plugins>
		<plugin>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-maven-plugin</artifactId>
			<version>9.2.11.v20150529</version>
			<configuration>
				<scanIntervalSeconds>10</scanIntervalSeconds>
				<webApp>
					<contextPath>/connect2tech.in-OnlineBanking</contextPath>
				</webApp>
			</configuration>
		</plugin>
	</plugins>
</build>
```

**mvn install**: Installing jars into local repository.

```
mvn install:install-file -Dfile=D:\naresh.chaurasia\asm-3.3.1.jar -DgroupId=com.jboss-custom -DartifactId=asm -Dversion=3.3.1 -Dpackaging=jar
```

**Creating an Executable JAR**: Create executable jar 

https://maven.apache.org/plugins/maven-assembly-plugin/usage.html

```
<project>
  [...]
  <build>
    [...]
    <plugins>
      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <version>3.2.0</version>
        <configuration>
          [...]
          <archive>
            <manifest>
              <mainClass>org.sample.App</mainClass>
            </manifest>
          </archive>
        </configuration>
        [...]
      </plugin>
      [...]
</project> 
```



**maven-source-plugin**: Generate source code jar for Maven based project

```
<build>
  <plugins>
	<plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-source-plugin</artifactId>
	<executions>
		<execution>
			<id>attach-sources</id>
			<goals>
				<goal>jar</goal>
			</goals>
		</execution>
	</executions>
   </plugin>
 </plugins>
</build>
```

